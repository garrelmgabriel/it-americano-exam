'use client'


import { get, ref, push, set, update, onValue, remove } from "firebase/database";
import { db } from "../lib/firebaseconfig";
import { ItemFormValues } from '@/components/menu/data/schema'
import { useState } from "react";


export const addItem = async (data: ItemFormValues) => {

  try {
    const usersRef = ref(db, 'items/' + data.code)
    // const newDataRef = push(usersRef);

    set(usersRef, data)

    window.location.assign('/itemmenu')

  } catch (error) {
    console.error("Error pushing data to Firebase: ", error);
    return false;
  }

}


export const deleteItem = async (id: any) => {
  try {
    const usersRef = ref(db, `items/${id}`)
    await remove(usersRef);
    window.location.assign('/itemmenu')
  } catch (error) {
    console.error("Error pushing data to Firebase: ", error);
    return false;
  }

}

