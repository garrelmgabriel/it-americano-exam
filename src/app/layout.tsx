import React from 'react'
import { Inter as FontSans } from "next/font/google"
import localFont from "next/font/local"
import { cn } from "@/lib/utils"

import './globals.css'
import { siteConfig } from '@/config/site'
import { ThemeProvider } from '@/components/theme-provider'

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
})

// const fontSans = localFont({
//   src: "../assets/fonts/Inter-Bold.ttf",
//   variable: "--font-sans",
// })



const fontHeading = localFont({
  src: "../assets/fonts/CalSans-SemiBold.ttf",
  variable: "--font-heading",
})



interface RootLayoutProps {
  children: React.ReactNode
}

export const metadata = {
  title: {
    default: siteConfig.name,
    template: `%s | ${siteConfig.name}`,
  },
  description: siteConfig.description,
  keywords: [
    "Next.js",
    "React",
    "Tailwind CSS",
    "Server Components",
    "Radix UI",
  ],
  authors: [
    {
      name: "axzeron.ai",
    },
  ],
  creator: "axzeron.ai",

}
export default function RootLayout({ children }: RootLayoutProps) {
  return (

    <html lang="en" suppressHydrationWarning>
      <head />
      <body
        className={cn(
          "min-h-screen bg-background font-sans antialiased",
          fontSans.variable,
          fontHeading.variable
        )}
      >
        <ThemeProvider
          attribute="class"
          defaultTheme="light"
          enableSystem
          disableTransitionOnChange
        >

          {children}

        </ThemeProvider>

      </body>
    </html>

  )
}
