import { AspectRatio } from "@/components/ui/aspect-ratio"
import { Icons } from "@/components/icons"
import Image from "next/image"
import Link from "next/link"
import { siteConfig } from "@/config/site"
import { cn } from "@/lib/utils"
import { buttonVariants } from "@/components/ui/button"
import { MainLogo } from "@/components/main-logo"
import { Toaster } from "@/components/ui/toaster"
import imaget from '../../assets/gray-2.jpg'
interface AuthLayoutProps {
  children: React.ReactNode
}

export default function AuthLayout({ children }: AuthLayoutProps) {

  return (
    <div className="grid min-h-screen grid-cols-1 overflow-hidden md:grid-cols-3 lg:grid-cols-2">

      <AspectRatio ratio={16 / 9}>

        <div className="absolute inset-0 bg-gradient-to-t from-background to-background/60 md:to-background/40" />
        {/* background and logo image */}
        <div className="relative">
          <div
            className="absolute inset-0 bg-cover bg-local "
            style={{
              backgroundImage:
                "url(src/app/gray-2.jpg)",
            }}
          />
          {/* main logo */}
          <Link
            href="/"
            className={cn(
              buttonVariants({ variant: "ghost" }),
              "absolute left-4 top-4 md:left-8 md:top-8"
            )}>
            <MainLogo color='black' className="m-2" />

          </Link>
        </div>

      </AspectRatio>
      <main className="container absolute top-1/2 col-span-1 flex -translate-y-1/2 items-center md:static md:top-0 md:col-span-2 md:flex md:translate-y-0 lg:col-span-1">
        {children}
        <Toaster />
      </main>
    </div>
  )
}
