import { Metadata } from "next"

// import { getCurrentUser } from "@/lib/session"
import { Shell } from "@/components/shells/shell"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card"
import { SignInForm } from "@/components/forms/signin-form"
// import { OAuthSignIn } from "@/components/auth/oauth-signin"
// import { SignInForm } from "@/components/forms/signin-form"

export const metadata: Metadata = {
  title: "Login"
}

export default async function LoginPage() {


  (
    true
      ? <>  const user = await getCurrentUser()
        const username = user?.name</>
      : null
  )

  return (

    <Shell className="max-w-sm">
      <Card>
        <CardHeader className="space-y-1">
          <CardTitle className="text-2xl">Sign in</CardTitle>
          <CardDescription>
            Sign in with your email and password
          </CardDescription>
        </CardHeader>
        <CardContent className="grid gap-4">

          <SignInForm />
        </CardContent>

      </Card>
    </Shell>
  )
}
