'use client'

import { motion } from "framer-motion"

const variants = {
    hidden: { opacity: 0 },
    enter: { opacity: 1 },
}

interface TemplateFileProps {
    children: React.ReactNode
  }
  
const Template = ({ children }:TemplateFileProps) => {

    return <>
        <motion.main variants={variants} initial='hidden' animate='enter'
            transition={{ type: 'linear', delay: 0.2, duration: 0.4 }}>
            {children}
        </motion.main>

        <div className="h-auto"></div>

    </>
}
export default Template
