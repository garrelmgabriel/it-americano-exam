import { DashboardComp } from '@/components/ion-gen-main-b';
import { SiteFooter } from '@/components/site-footer';
import { SiteHeader } from '@/components/site-header';

import { Metadata } from 'next';

export const metadata: Metadata = {
    title: "IT Exam | Bringing IT to the next level"
}
const MainPage = () => {
    return (
        <div className="relative flex min-h-screen flex-col">
            <SiteHeader />
                <DashboardComp />
            <SiteFooter />
        </div>
    )
}

export default MainPage
