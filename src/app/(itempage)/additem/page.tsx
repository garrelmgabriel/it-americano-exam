'use client'

import { Icons } from '@/components/icons'
import { ItemFormValues, itemSchema } from '@/components/menu/data/schema'
import { Button, buttonVariants } from '@/components/ui/button'
import { Card, CardContent, CardFooter } from '@/components/ui/card'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/ui/form'
import { Input } from '@/components/ui/input'
import { Textarea } from '@/components/ui/textarea'
import { addItem } from '@/hooks/use-firebase'
import { cn } from '@/lib/utils'
import { faker } from "@faker-js/faker"
import { zodResolver } from "@hookform/resolvers/zod"
import Link from 'next/link'
import { useForm } from "react-hook-form"

const AddItem = () => {

    const defaultValues: Partial<ItemFormValues> = {
        code: `TODO-${faker.number.int({ min: 1000, max: 9999 })}`,
    }
    const form = useForm<ItemFormValues>({
        resolver: zodResolver(itemSchema),
        mode: "onChange",
        defaultValues,
    })


    const save_item = (data: ItemFormValues) => {
        addItem(data)

    }
    return (
        <>
            <div className='p-[-20px]'>
                <Link
                    href="/itemmenu"
                    className={cn(
                        buttonVariants({ variant: "ghost" }),
                    )}>
                    <>
                        <Icons.chevronLeft className="mr-2 h-4 w-4" />
                        Back
                    </>
                </Link>
            </div>
            <div className="h-full flex-1 flex-col space-y-8 md:flex">
                <div className="flex items-center justify-between space-y-2">
                    <div>
                        <h2 className="text-2xl font-bold tracking-tight">Add New Todo</h2>
                    </div>
                </div>
                <Card className="w-full">
                    {/* FORM */}
                    <Form {...form} >
                        <form
                            className="grid gap-4"
                            onSubmit={form.handleSubmit(save_item)}
                        >



                            <CardContent className='p-5'>
                                <div className="flex flex-row">


                                    {/* item name */}
                                    <FormField
                                        control={form.control}
                                        name="name"
                                        render={({ field }) => (
                                            <FormItem className='w-full pl-2 '>
                                                <FormLabel>Todo Name</FormLabel>
                                                <FormControl>
                                                    <Input placeholder="Give it a name..." {...field} />
                                                </FormControl>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                </div>

                                <div className="flex flex-row">


                                    {/* item name */}
                                    <FormField
                                        control={form.control}
                                        name="comment"
                                        render={({ field }) => (
                                            <FormItem className='w-full pl-2 '>
                                                <FormLabel>Comment</FormLabel>
                                                <FormControl>
                                                    <Textarea
                                                        placeholder="Tell us a little bit about your todo"
                                                        className="resize-none"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                </div>


                            </CardContent>
                            <CardFooter className="flex justify-start pl-5">
                                <Button className="mt-2" type="submit">
                                    Create
                                </Button>
                            </CardFooter>

                        </form>
                    </Form>
                    {/* Form */}
                </Card>
            </div>
        </>

    )
}

export default AddItem
