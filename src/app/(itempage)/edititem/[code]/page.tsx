'use client'

import Editportal from '@/components/editportal'
import { Icons } from '@/components/icons'
import { buttonVariants } from '@/components/ui/button'
import { cn } from '@/lib/utils'
import Link from 'next/link'

const EditItem = (params:any) => {

    // const defaultValues: Partial<ItemFormValues> = {
    //     code: `ITEM-${faker.number.int({ min: 1000, max: 9999 })}`,
    //     options: ["null"],
    // }

    // const form = useForm<ItemFormValues>({
    //     resolver: zodResolver(itemSchema),
    //     mode: "onChange",
    //     defaultValues,
    // })

    return (
        <>
            <div className='p-[-20px]'>
                <Link
                    href="/itemmenu"
                    className={cn(
                        buttonVariants({ variant: "ghost" }),
                    )}>
                    <>
                        <Icons.chevronLeft className="mr-2 h-4 w-4" />
                        Back
                    </>
                </Link>
            </div>
            {/* Edit Fields */}
            <Editportal code={params.params.code} />
        </>

    )
}

export default EditItem
