import MainNav from '@/components/main-nav'
import { SiteFooter } from '@/components/site-footer'
import { siteConfig } from '@/config/site'
import Link from 'next/link'
import { cn } from '@/lib/utils'
import { buttonVariants } from '@/components/ui/button'
import { Icons } from '@/components/icons'

interface TestLayoutProps {
  children: React.ReactNode
}


export const metadata = {
  title: {
    default: "Item List",
    template: `Item Management - %s - ${siteConfig.name}`,
  }
}


export default function TestLayout({ children }: TestLayoutProps) {
  return (
    <div className="flex min-h-screen flex-col space-y-6 ">
      <header className="sticky top-0 z-40 border-b bg-[#1d1d1d]">
        <div className="container flex h-16 items-center justify-between py-4  ">
          <MainNav title='Test Page' />
        </div>
      </header>

      <div className="container flex-1 gap-12 md:grid-cols-[200px_1fr]">
        <main className="flex w-full flex-1 flex-col overflow-hidden">
          {children}
        </main>
      </div>
      <SiteFooter className="border-t" />
    </div>
  )


}
