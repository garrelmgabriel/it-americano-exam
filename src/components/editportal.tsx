import { categories, options } from '@/components/menu/data/data'
import { ItemFormValues, itemSchema } from '@/components/menu/data/schema'
import { Button, buttonVariants } from '@/components/ui/button'
import { Card, CardContent, CardFooter } from '@/components/ui/card'
import { Checkbox } from '@/components/ui/checkbox'
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from '@/components/ui/form'
import { Input } from '@/components/ui/input'
import { db } from '@/lib/firebaseconfig'
import { cn } from '@/lib/utils'
import { faker } from "@faker-js/faker"
import { zodResolver } from "@hookform/resolvers/zod"
import { get, ref, update } from 'firebase/database'
import { ChevronDownIcon } from 'lucide-react'
import { useEffect, useState } from 'react'
import { useForm } from "react-hook-form"
import { Textarea } from './ui/textarea'


const Editportal = (props: any) => {
    const [ismounted, setisMounted] = useState(false);
    useEffect(() => {
        getPerItem()
        setTimeout(() => {
            setisMounted(true)
        }, 1000)
    }, [props.code])

    const usersRef = ref(db, 'items/' + props.code)

    const getPerItem = async () => {
        
        await get(usersRef).then((snapshot) => {
            if (snapshot.exists()) {
                form.setValue('code', snapshot.val().code)
                form.setValue('comment', snapshot.val().comment)
                form.setValue('name', snapshot.val().name)
            }
        }).catch((error) => {
            console.log(error)
        }
        )
    }
    const form = useForm<ItemFormValues>({
        resolver: zodResolver(itemSchema),
        mode: "onChange",
    })


    const updateData=(data:ItemFormValues)=>{
        try {
            update(usersRef,data)
            window.location.assign('/itemmenu')
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <div className="h-full flex-1 flex-col space-y-8 md:flex">
            <div className="flex items-center justify-between space-y-2">
                <div>
                    <h2 className="text-2xl font-bold tracking-tight">Edit Your Todo</h2>
                </div>
            </div>

            {
                ismounted ? <Card className="w-full">
                    {/* FORM */}
                    <Form {...form} >
                        <form
                            className="grid gap-4"
                        onSubmit={form.handleSubmit(updateData)}
                        >
                            <CardContent className='p-5'>
                                 {/* item name */}
                                 <FormField
                                        control={form.control}
                                        name="name"
                                        render={({ field }) => (
                                            <FormItem className='w-full pl-2 '>
                                                <FormLabel>Name</FormLabel>
                                                <FormControl>
                                                    <Input placeholder="Give it a name..." {...field} />
                                                </FormControl>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    /> 
                                    
                                     {/* item name */}
                                    <FormField
                                        control={form.control}
                                        name="comment"
                                        render={({ field }) => (
                                            <FormItem className='w-full pl-2 '>
                                                <FormLabel>Comment</FormLabel>
                                                <FormControl>
                                                <Textarea
                                                        placeholder="Tell us a little bit about your todo"
                                                        className="resize-none"
                                                        {...field}
                                                    />
                                                </FormControl>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />

                            </CardContent>
                            <CardFooter className="flex justify-start pl-5">
                                <Button className="mt-2" type="submit">
                                    Update
                                </Button>
                            </CardFooter>

                        </form>
                    </Form>
                    {/* Form */}
                </Card>
                    :
                    ""
            }

        </div>
    )
}

export default Editportal
