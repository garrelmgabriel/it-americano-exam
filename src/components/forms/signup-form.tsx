"use client"

import React, { useEffect } from 'react'
import { Button } from "@/components/ui/button"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"
// import { authSchema } from "@/lib/validations/register"


import {
    Form,
    FormControl,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { Input } from '../ui/input'
import { PasswordInput } from "@/components/password-input"
import { Icons } from "@/components/icons"
import { toast } from '@/components/ui/use-toast'

export const authSchema = z.object({
    email: z.string().email({
      message: "Please enter a valid email address",
    }),
    password: z
      .string()
      .min(8, {
        message: "Password must be at least 8 characters long",
      })
      .max(100)
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/, {
        message:
          "Password must contain at least 8 characters, one uppercase, one lowercase, one number and one special character",
      }),
  })


type AuthFormValues = z.infer<typeof authSchema>

export const SignUpForm = () => {
    const [isPending, startTransition] = React.useTransition()

    const form = useForm<AuthFormValues>({
        resolver: zodResolver(authSchema),
        defaultValues: {
            email: "",
            password: "",
        },
    })

    function onSubmit(data: AuthFormValues) {
        console.log('TEST')
        toast({
            title: "You submitted the following values:",
            description: (
                <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
                    <code className="text-white">{JSON.stringify(data, null, 2)}</code>
                </pre>
            ),
        })
    }


    const signUpEvent = async (data: AuthFormValues) => {

        startTransition(async () => {
            // try {
            //   const result = await signIn.create({
            //     identifier: data.email,
            //     password: data.password,
            //   })
      
            //   if (result.status === "complete") {
            //     await setActive({ session: result.createdSessionId })
      
            //     router.push(`${window.location.origin}/`)
            //   } else {
            //     /*Investigate why the login hasn't completed */
            //     console.log(result)
            //   }
            // } catch (err) {
            //   catchClerkError(err)
            // }
          })
    
        // const res = await fetch(
        //   `${thisOrigin}/api/posts`, {
        //   method: "POST",
        //   headers: {
        //     "Content-Type": "application/json",
        //   },
        //   body: JSON.stringify({
        //     title: data.title,
        //     content: data.content
        //   }),
    
        // }
        // )
    
        // if (res.status == 200) {
        //   setIsSaving(false)
        //   toast({
        //     description: "Your post has been saved.",
        //   })
        // }
        // router.refresh();
      }
    

    useEffect(() => { signUpEvent }, [])

    return (
        <Form {...form}>
            <form
                className="grid gap-4"
                onSubmit={form.handleSubmit(onSubmit)}
            >
                <FormField
                    control={form.control}
                    name="email"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Email</FormLabel>
                            <FormControl>
                                <Input placeholder="juan.kamote@gmail.com" {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                />
                {/* <FormField
                    control={form.control}
                    name="password"
                    render={({ field }) => (
                        <FormItem>
                            <FormLabel>Password</FormLabel>
                            <FormControl>
                                <PasswordInput placeholder="**********" {...field} />
                            </FormControl>
                            <FormMessage />
                        </FormItem>
                    )}
                /> */}
                <Button type="submit" disabled={isPending}>
                    {isPending && (
                        <Icons.spinner
                            className="mr-2 h-4 w-4 animate-spin"
                            aria-hidden="true"
                        />
                    )}
                    Sign up
                    <span className="sr-only">Sign up</span>
                </Button>
            </form>
        </Form>
    )
}


