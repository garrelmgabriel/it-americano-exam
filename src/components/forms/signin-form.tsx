"use client"

import React, { useEffect } from 'react'
import { Button } from "@/components/ui/button"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import type { z } from "zod"
import { checkEmailSchema } from "@/lib/validations/register"
import { toast } from '@/components/ui/use-toast'

import {
    Form,
    FormControl,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { Input } from '../ui/input'
import { PasswordInput } from "@/components/password-input"
import { Icons } from "@/components/icons"
import { cn } from '@/lib/utils'

type FormValues = z.infer<typeof checkEmailSchema>
export const SignInForm = () => {
    const [isPending, startTransition] = React.useTransition()
    const [isSaving, setIsSaving] = React.useState<boolean>(false)
    const form = useForm<FormValues>({
        resolver: zodResolver(checkEmailSchema),
        defaultValues: {
            email: "",
        },
    })

    function onSubmit(data: FormValues) {

      if(data.email=='johndoe@gmail.com' || data.password=='Admin@1234'){
        window.location.assign('/itemmenu')
      }else if(data.email=='juandoe@gmail.com' || data.password=='Admin@1234'){
        window.location.assign('/itemmenu')
      }else{
        toast({
          description: (
              <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
                  <code className="text-white">Invalid username or password.</code>
              </pre>
          ),
      })
      }
        console.log("here")
        setIsSaving(true);
        
    }



    const signInEvent = async (data: FormValues) => {
        console.log("here")
        setIsSaving(true);
        toast({
            title: "You submitted the following values:",
            description: (
                <pre className="mt-2 w-[340px] rounded-md bg-slate-950 p-4">
                    <code className="text-white">{JSON.stringify(data, null, 2)}</code>
                </pre>
            ),
        })
        setIsSaving(false);

    }
    useEffect(() => { signInEvent }, [])

    return (
        <div className="mx-auto flex w-full flex-col justify-center space-y-6 sm:w-[350px]">

        <div className={cn("grid gap-6")}>
          <Form {...form} >
            <form
              className="grid gap-4"
              onSubmit={(...args) => void form.handleSubmit(onSubmit)(...args)}
            >
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem  >
                    <FormLabel className='text-primary' >Email</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Your Email" {...field} className=' border-primary' />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <FormItem className='text-primary'>
                    <FormLabel>Password</FormLabel>
                    <FormControl>
                      <PasswordInput placeholder="**********" {...field} className='border-primary' />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <Button type="submit" >
  
                {/* {isprocessing && (
                  <Icons.spinner
                    className="mr-2 h-4 w-4 animate-spin"
                    aria-hidden="true"
                  />
                )} */}
                Sign in
                <span className="sr-only">Sign in</span>
              </Button>
            </form>
          </Form>
        </div>
      </div>
    )
}


