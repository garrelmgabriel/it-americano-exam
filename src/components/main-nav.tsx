"use client"

import { cn } from "@/lib/utils";
import { MainNavItem } from '@/types';
import Link from "next/link";
import { useSelectedLayoutSegment } from "next/navigation";
import React from 'react';
import { MainLogo } from './main-logo';


import {
    NavigationMenu,
    NavigationMenuItem,
    NavigationMenuLink,
    NavigationMenuList
} from "@/components/ui/navigation-menu";

import { navigationMenuTriggerStyle } from "@/components/ui/navigation-menu";

interface MainNavProps {
    items?: MainNavItem[]
    title?: string
    children?: React.ReactNode
    session?: any
}

const MainNav = ({ items, children, title, session }: MainNavProps) => {
    const segment = useSelectedLayoutSegment()
    const [showMobileMenu, setShowMobileMenu] = React.useState<boolean>(false)


    return (
        <>
            <div className="flex gap-6 md:gap-10">
                <Link href="/" className="flex items-center space-x-2">
                    <MainLogo className='mxd:hidden' color='white'/>
                </Link>

                {/* <NavigationMenu className="hidden gap-6 md:flex" >
                    <NavigationMenuList>
                        {items?.filter((item) => item.hidden != true).map((item, index) => (
                            <NavigationMenuItem key={index}>
                                <Link
                                    legacyBehavior passHref
                                    href={item.disabled ? "#" : item.href}
                                >
                                    <NavigationMenuLink className={
                                        cn(
                                            "items-center text-lg font-medium transition-colors hover:text-foreground/80 sm:text-sm",
                                            item.href.startsWith(`/${segment}`)
                                                ? "text-foreground"
                                                : "text-foreground/60",
                                            item.disabled && "cursor-not-allowed opacity-80",
                                            navigationMenuTriggerStyle()
                                        )
                                    }>
                                        {item.title}
                                    </NavigationMenuLink>
                                </Link>
                            </NavigationMenuItem>
                        ))}
                    </NavigationMenuList>
                </NavigationMenu> */}

            </div>

        </>


    )
}

export default MainNav
