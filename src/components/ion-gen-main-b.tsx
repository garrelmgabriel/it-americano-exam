'use client'

import { buttonVariants } from '@/components/ui/button';
import { cn } from '@/lib/utils';
import Link from 'next/link';


export const DashboardComp = () => {
    return (
        <main className="flex-1">
            <section className="space-y-6 pb-8 pt-6 md:pb-12 md:pt-10 lg:py-32">
                <div className="container flex max-w-[64rem] flex-col items-center gap-4 text-center">
                    <h1 className="animate-fade-up bg-gradient-to-br from-primary to-muted-primary font-heading bg-clip-text  tracking-[-0.02em] text-transparent opacity-1 drop-shadow-sm mxd:grid mxd:grid-cols-1 font-bold text-3xl sm:text-5xl md:text-6xl lg:text-6xl">
                        <span>IT Americano</span>
                    </h1>
                    <p className="max-w-[50rem] leading-normal text-muted-foreground sm:text-xl sm:leading-8">
                        We achieve excellence by inspiring growth among our employees, challenging them to reach their maximum potential, and inspiring them to be the best version of themselves. Doing this enables us to outperform our competitors and provide the best−in−class services and solutions.
                    </p>
                    <div className="space-x-4">
                        <Link href="/login" className={cn(buttonVariants({ size: "lg" }))}>
                            Try it out!
                        </Link>
                    </div>
                </div>
            </section>
        </main>
    )
}
