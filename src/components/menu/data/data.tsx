
export const categories = [
  {
    value:'ricemeal',
    name:'Rice Meal'
  },
  {
    value:'pasta',
    name:'Pasta'
  },
  {
    value:'sandwich',
    name:'Sandwich'
  },
  {
    value:'sides',
    name:'Sides'
  },
  {
    value:'desserts',
    name:'Desserts'
  },
  {
    value:'drinks',
    name:'Drinks'
  },
  {
    value:'others',
    name:'Others'
  },

]
export const options = [
  {
    value:'null',
    name:'None'
  },
  {
    value:'small',
    name:'Small'
  },
  {
    value:'regular',
    name:'Regular'
  },
  {
    value:'medium',
    name:'Medium'
  },
  {
    value:'large',
    name:'Large'
  },
  {
    value:'solo',
    name:'Solo'
  },
  {
    value:'pthigh',
    name:'Thigh Part'
  },
  {
    value:'pwing',
    name:'Wing Part'
  },
  {
    value:'pbreast',
    name:'Breast Part'
  },

] as const
