import fs from "fs"
import path from "path"
import { faker } from "@faker-js/faker"

import { categories } from "./data"

// Function to generate incremental numbers

const arrLenght:number= 5;

function generateIncrementalNumbers(start: number, step: number) {
  let current = start;
  return function() {
    const value = current;
    current += step;
    return value;
  };
}

const getNextIncrementalNumber = generateIncrementalNumbers(1, 1);


const items = Array.from({ length: 20}, () => ({
  // id: getNextIncrementalNumber(),
  code: `ITEM-${faker.number.int({ min: 1000, max: 9999 })}`,
  name: faker.commerce.productName(),
  category:faker.helpers.arrayElement(categories).name,
  options: faker.commerce.productName(),
  price: faker.number.int({ min: 100, max: 300 }),
  cost: faker.number.int({ min: 100, max: 300 }),
  stock: faker.number.int({ min: 100, max: 500 }),
}))

fs.writeFileSync(
  path.join(__dirname, "items.json"),
  JSON.stringify(items, null, 2)
)

console.log("✅ Tasks data generated.")
