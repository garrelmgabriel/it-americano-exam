import { highLightConfig } from "@/config/highlight"
import MainNav from "./main-nav"


export const SiteHeader = ({ user }: any) => {
    return (
        <header className="sticky top-0 z-50 w-full border-b bg-[#1d1d1d]">
            <div className="container flex h-16 items-center">
                <MainNav items={highLightConfig.mainNav} session={user} />
            </div>
        </header>
    )
}