'use client'

import React, { useEffect, useState } from 'react'
import { DataTable } from './menu/data-table'
import { columns } from "@/components/menu/columns"
import { get, ref } from 'firebase/database'
import { db } from '@/lib/firebaseconfig'
import { cn } from '@/lib/utils'
import { EmptyPlaceholder } from './empty-placeholder'
import { Icons } from './icons'
import { buttonVariants } from './ui/button'
import Link from 'next/link'
const Itemportal = () => {
    const [itemData, setItemData] = useState([]);
    const [ismounted, setisMounted] = useState(false);

    useEffect(() => {
        getItems()
        setTimeout(() => {
            setisMounted(true)
        }, 1000)
    })

    const getItems = async () => {
        const usersRef = ref(db, 'items')
        // let ItemArray: any = []

        await get(usersRef).then((snapshot) => {
            if (snapshot.exists()) {
                const ItemArray: any = Object.keys(snapshot.val()).map(key => {
                    const item = snapshot.val()[key];
                    return {
                        id: key,
                        code:item.code,
                        name: item.name,
                        comment: item.comment,
                    };
                })
                setItemData(ItemArray)
            } else {
                return 'No data'
            }
        }).catch((error) => {
            console.log(error)
        }
        )
    }

    return (
        <div className="h-full flex-1 flex-col space-y-8 md:flex">
            {itemData?.length != 0 ? (
                <>
                    <div className="flex items-center justify-between space-y-2">
                        <div>
                            <h2 className="text-2xl font-bold tracking-tight">Welcome back!</h2>
                            <p className="text-muted-foreground">
                                Here&apos;s a list of your todos for this month!
                            </p>
                        </div>
                    </div>
                    <DataTable data={itemData!} columns={columns} />
                </>
            )
                :
                (
                    ismounted ?
                        <EmptyPlaceholder>
                            <EmptyPlaceholder.Icon name="post" />
                            <EmptyPlaceholder.Title>No todo on the list</EmptyPlaceholder.Title>
                            <EmptyPlaceholder.Description>
                                You don&apos;t have any todo yet. Start creating content.
                            </EmptyPlaceholder.Description>
                            <Link
                                className={cn(
                                    buttonVariants({ variant: "outline" })
                                )}
                                href={"/additem"}            >
                                <Icons.add className="mr-2 h-4 w-4" />
                                Add Todo
                            </Link>
                        </EmptyPlaceholder> :
                        <></>
                )}

        </div>
    )
}

export default Itemportal
