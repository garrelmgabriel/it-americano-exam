'use client'

import * as DialogPrimitive from "@radix-ui/react-dialog"

import { Button } from './ui/button'
import {
    DialogContent,
    DialogDescription,
    DialogFooter,
    DialogHeader,
    DialogTitle
} from "./ui/dialog"

import { deleteItem } from "@/hooks/use-firebase"


export const ConfirmModal = (props: any) => {


    return (
        <DialogContent >
            <DialogHeader>
                <DialogTitle>Are you absolutely sure?</DialogTitle>
                <DialogDescription>
                    By clicking the "Delete" button below, this todo will be remove permanently. Please ensure before proceeding.
                </DialogDescription>
            </DialogHeader>
            <DialogFooter>
                <Button type="button" variant='secondary' className="my-2" asChild>
                    <DialogPrimitive.Close >
                        Cancel
                    </DialogPrimitive.Close>
                    {/* <Button
                        type='button'
                        onClick={() => close_modal(false)}
                        variant="secondary">
                        Cancel
                    </Button> */}

                </Button>

                <Button
                    type="button"
                    onClick={() => {
                        deleteItem(props.data.id);

                    }}
                    className="my-2"
                >
                    <DialogPrimitive.Close>
                        Delete
                    </DialogPrimitive.Close>
                </Button>

            </DialogFooter>
        </DialogContent>
    )
}

