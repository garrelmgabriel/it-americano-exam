"use client"

import logowhite from '@/app/logo-it-white.svg';
import logoblack from '@/app/logo-it.svg';
import React from 'react';

import { useTheme } from 'next-themes';
import Image from 'next/image';

export const MainLogo = (props: any) => {

    const { theme } = useTheme()
    return (
        <>
            <Image
                src={props.color == 'white' ? logowhite : logoblack}
                height={30}
                width={30}
                alt={`UtakPOS`}
            />

        </>
    )
}


