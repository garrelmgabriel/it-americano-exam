import { SiteConfig } from "@/types";


export const siteConfig: SiteConfig = {
    name: "IT Exam",
    description:
      "We achieve excellence by inspiring growth among our employees, challenging them to reach their maximum potential, and inspiring them to be the best version of themselves. Doing this enables us to outperform our competitors and provide the best−in−class services and solutions.",
    }
  