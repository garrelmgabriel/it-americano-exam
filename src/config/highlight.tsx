import { HighlightConfig } from "@/types";


export const highLightConfig: HighlightConfig = {
  mainNav: [
    {
      title: "Highlight",
      href: "/",
      disabled: true,
      hidden: true
    },
    {
      title: "Features",
      href: "/#features",
    },
    {
      title: "Meet Us",
      href: "/#meetus",
      disabled: true,
      hidden: true
    },
  
  ],
  
}

export const dashboardConfig: HighlightConfig = {
  mainNav: [
    {
      title: "Test Page",
      href: "/testmongo",
    },
  
  ],
}
