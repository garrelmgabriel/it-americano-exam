
import { initializeApp } from "firebase/app";
import { getDatabase} from "firebase/database"

const firebaseConfig = {
  apiKey: "AIzaSyCvoQKXJLGpjoW5T9lxwXbtFC5c_ht78JE",
  authDomain: "it-exam-bb75d.firebaseapp.com",
  databaseURL: "https://it-exam-bb75d-default-rtdb.firebaseio.com",
  projectId: "it-exam-bb75d",
  storageBucket: "it-exam-bb75d.appspot.com",
  messagingSenderId: "561231006309",
  appId: "1:561231006309:web:91f196434178a55c08d48c"
};


const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);